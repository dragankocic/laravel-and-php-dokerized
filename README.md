# Docker & Kubernetes
## Section 8 - A More Complex Setup: A Laravel & PHP Dockerized Project



## About

**Course** : [Docker & Kubernetes: The Practical Guide [2023 Edition]](https://www.udemy.com/course/docker-kubernetes-the-practical-guide/)

**Instructor** : [Maximilian Schwarzmüller](https://www.udemy.com/user/maximilian-schwarzmuller/)
